function default_props(object, default_props={}){
    for (let key in default_props){
        if(object[key] === undefined){
            object[key] = default_props[key];
        }
    }
    return object;
}

module.exports = default_props;