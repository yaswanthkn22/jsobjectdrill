

function inverse(object = {}){
    let new_obj = {};
    for (let key in object){
        new_obj[object[key]] = key;
    }
    return new_obj;
}

module.exports = inverse;