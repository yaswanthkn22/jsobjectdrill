
function keys(object){
    let result = [];
    for (let key in object){
        result.push(key);
    }
    return result;
}

module.exports = keys;
