function pairs(object){
    let result = []

    for (let key in object){
        let temp = []
        temp.push(key);
        temp.push(object[key]);
        result.push(temp);
        temp = []
    }

    return result;
}

module.exports = pairs;